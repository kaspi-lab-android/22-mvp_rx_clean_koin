package kz.company.mvp.ui

import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kz.company.common.base.BasePresenter
import kz.company.mvp.api.ExchangeApiService
import kz.company.mvp.model.Currency

class SamplePresenter(
    private val exchangeApiService: ExchangeApiService
) :
    BasePresenter<SampleContract.View>(),
    SampleContract.Presenter {

    override fun load() {
        exchangeApiService.get()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { currency: Currency ->
                    view?.show(currency)
                },
                {
                    Log.d("Error", "To load exchange")
                }
            )
            .disposeOnCleared()
    }

}