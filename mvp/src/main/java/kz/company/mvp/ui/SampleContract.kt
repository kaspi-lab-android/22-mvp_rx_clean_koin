package kz.company.mvp.ui

import kz.company.common.base.MvpPresenter
import kz.company.common.base.MvpView
import kz.company.mvp.model.Currency

interface SampleContract {

    interface View: MvpView{
        fun show(currency: Currency)
        fun showInfo()
    }

    interface Presenter: MvpPresenter<View>{
        fun load()
    }
}