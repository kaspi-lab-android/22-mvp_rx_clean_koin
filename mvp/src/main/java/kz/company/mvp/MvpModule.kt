package kz.company.mvp

import kz.company.common.base.InjectionModule
import kz.company.mvp.api.ExchangeApiService
import kz.company.mvp.ui.SamplePresenter
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object MvpModule : InjectionModule {
    override fun create(): Module = module {
        viewModel { SamplePresenter(get()) }
        single { ExchangeApiService.create() }
    }
}