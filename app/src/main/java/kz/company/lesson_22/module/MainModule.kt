package kz.company.lesson_22.module

import kz.company.common.base.InjectionModule
import org.koin.core.module.Module
import org.koin.dsl.module

object MainModule : InjectionModule {
    override fun create(): Module = module {
        single { SampleC() }
        single { SampleD() }
        single { SampleB(get()) }
        single { SampleA(get(), get()) }
    }
}