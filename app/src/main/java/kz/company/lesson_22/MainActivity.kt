package kz.company.lesson_22

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kz.company.mvp.ui.SampleFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.mainContainer, SampleFragment())
            .commit()
    }
}