package kz.company.lesson_22

import kz.company.lesson_22.module.MainModule
import kz.company.mvp.MvpModule
import org.koin.core.module.Module

object KoinModules {
    val modules: List<Module> =
        listOf(
            MainModule.create(),
            MvpModule.create()
        )
}